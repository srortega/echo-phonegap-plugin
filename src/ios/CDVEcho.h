//
//  CDVEcho.h
//
//  Created by Sergio Ortega on 13/03/14.
//  Copyright (c) 2014 Sergio Ortega. All rights reserved.
//


#import <Cordova/CDV.h>

@interface CDVEcho : CDVPlugin

- (void)echo:(CDVInvokedUrlCommand*)command;

@end
