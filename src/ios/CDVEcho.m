//
//  CDVEcho.m
//
//  Created by Sergio Ortega on 13/03/14.
//  Copyright (c) 2014 Sergio Ortega. All rights reserved.
//

#import "CDVEcho.h"
#import <Cordova/CDV.h>

@interface CDVEcho ()

@end

@implementation CDVEcho


- (void)echo:(CDVInvokedUrlCommand*)command
{
	NSLog(@"entering CDV::echo method");
    CDVPluginResult* pluginResult = nil;
    NSString* echo = [command.arguments objectAtIndex:0];
    
    NSMutableString *reversedString = [NSMutableString string];
    NSInteger charIndex = [echo length];
    while (echo && charIndex > 0) {
        charIndex--;
        NSRange subStrRange = NSMakeRange(charIndex, 1);
        [reversedString appendString:[echo substringWithRange:subStrRange]];
    }
    
    NSLog(@"%@", reversedString);
    
    
    if (reversedString != nil && [reversedString length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:reversedString];
        NSLog(@"this plugin is echoing your words");
        
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        NSLog(@"this plugin is NOT echoing your words");
    }
    
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

    
    /*
    if (echo != nil && [echo length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:echo];
        NSLog(@"this plugin is echoing your words");

    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        NSLog(@"this plugin is NOT echoing your words");
    }
    
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    */
    
}

@end
