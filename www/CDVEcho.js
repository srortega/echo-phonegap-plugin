'use strict';

var CDVEcho = function() {
	this.defaultString = 'This is the default string';
};

CDVEcho.prototype.echo = function(words) {
	words = words ? words : this.defaultString;
	console.log('trying to echo "' + words +'" to iOS');
	var on_success = function(param) {
		console.log('success cordova callback');
		console.log(param);
	};
	var on_error = function(error) {
		console.log('error cordova callback');
		console.log(error);
	};
	return Cordova.exec(on_success, on_error, 'CDVEcho', 'echo', [words]);
};